# מבנה הקבצים בלינוקס #
בכל תקייה במערכת לינוקס קיימות 2 תקייות וירטואליות
* `.`
  התקייה הזאת היא אותה תיקייה שבה אנחנו נמצאים כלומר  
  `dir/.` = `dir`
* `..`
  התקייה הזאת היא תקיית כלומר התקייה שמכילה את התקייה שבה אנחנו נמצאים כלומר    
  `dir/sub_dir/..` = `dir`

# פקודות לינוקס #
במחשב הלינוקס קיימת תוכנה שנקראת terminal את הטרמינל ניתן לפתוח על ידי מקש ימני בתקייה כלשהי
(או בשולחן העבודה שהוא גם סוג של תקייה)
כאשר הטרמינל יפתח הוא יראה כך
```console
$
```
בדרך כלל כשיהיה כתוב שאפשר להכניס שם של קובץ, יהיה ניתן גם להכניס כמה שמות של קבצים  
בדרך כלל יהיה ניתן גם להכניס שם של קובץ עם כוכביות וסימני שאלה כך ש:  
* סימן שאלה  
  סימן שאלה אומר שיכול להיות כל תו
* כוכבית  
  כוכבית אומרת שיכולים להיות כל מיני תווים במקום הכוכבית. 

לדוגמה עבור  
`a*de?g`  
מתאים:  
* abcdefg
* adefg
* adeag

לא מתאים:  
* abcdeg
* abcdeffg


בהסברים הבאים הולכים להיות דוגמות לכל מיני פקודות בטרמינל  
בכל דוגמה בשורה שבה מופיע `$` בתחילת השורה, משמעותה הרצת פקודה  
בכל משורה שמתחילה ב `>` משמעותה הכנסת קלט מהמשתמש  
כל שורה שמתחילה אחרת משמעותה פלט של הטרמינל  
  
לפקודות יכולים להיות "דגלים" דגלים זה סוג של קלט של הפקודה בדרך כלל דגלים יתחילו במקף אחד אם הם בעלי אות אחת, או בשני מקפים אם הם מילה.  
נגיע לדוגמות בהמשך.
## `whoami` (who am I?!) ##
הפקודה תחזיר את שם המשתמש שלכם
```console
$ whoami
hodva
```
כלומר שם המשתמש שלי הוא
hodva

## `man` (manual) ##
כדי לקבל הסבר על פקודת לינוס/פעולת סי פלוס פלוס. ניתן להשתמש בפקודה man
```console
$ man whoami
WHOAMI(1)                        User Commands                       WHOAMI(1)

NAME
       whoami - print effective userid

SYNOPSIS
       whoami [OPTION]...

DESCRIPTION
       Print  the  user  name  associated  with the current effective user ID.
       Same as id -un.

       --help display this help and exit

       --version
              output version information and exit

AUTHOR
       Written by Richard Mlynarik.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/> 
       Report   whoami   translation    bugs    to    <https://translationpro‐
       ject.org/team/>

COPYRIGHT
       Copyright  ©  2018  Free Software Foundation, Inc.  License GPLv3+: GNU
       GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
       This is free software: you are free  to  change  and  redistribute  it.
       There is NO WARRANTY, to the extent permitted by law.

SEE ALSO
       Full documentation at: <https://www.gnu.org/software/coreutils/whoami>
       or available locally via: info '(coreutils) whoami invocation'

GNU coreutils 8.30                 July 2018                         WHOAMI(1)
```


## `pinky2` ##
על מנת לדעת פרטים על משתמש מסויים נשתמש בפקודה
pinky2  
דוגמה:
```console
$ whoami
hodva
$ pinky2 hodva
Login name: hodva                       In real life:  Hod Vaknin
Directory: /cs/stud/hodva               Shell:  /bin/tcsh
```

## `pwd` (print working directory) ##
הפקודה תציג לכם היכן בדיוק הטרמינל פתוח  
לדוגמה:  
```console
$ pwd
home/my_directory
```


## `cd` (change directory) ##
הפקודה הזאת תשנה את המיקום שבו הטרמינל פתוח.  
אם נשים שם של תקייה שמצאת בתקייה בה הטרמינל פתוח, לשם הטרמינל יעבור לדוגמה
```console
$ pwd
home/directory
$ ls
subdir1
subdir2
$ cd subdir1
$ pwd
home/directory/subdir1
```
הסבר על הדוגמה:  
נניח ופתחנו טרמינל בתקייה `home/directory` בתקייה הזאת קיימים שני תיקיות   
* subdir1
* subdir2

בהתחלה בדקנו איפה אנחנו נמצאים וקיבלנו את התקייה בה פתחנו את הטרמינל  
לאחר מכן בדקנו איזה עוד קבצים ותקייות יש וקיבלנו את שני התיקיות  
לאחר מכן עברנו לתקייה הפנימית  
כשבדנקו שוב איפה אנחנו נמצאים, קיבלנו את התקייה הפנימית

אם נשים `..` במקום שם של תקייה המעבר יהיה לתקיית 'אם' של אותה תיקייה לדוגמה:  
```console
$ pwd
home/directory/subdir1
$ cd ..
$ pwd
home/directory
```
בדוגמה הזאת היינו בתיקייה  
`home/directory/subdir1`  
ועברנו לתקייה  
`home/directory`


## `cat` (Concatenate) ##
הצגת כל הקובץ לדוגמה 
```console
$ cat months.txt
January
February
March
April
May
June
July
August
September
October
November
December
```
בקובץ קיימים כל החודשים לפי הסדר.  

### `head` / `tail` ###
הצגת 10 השורות הראשונות או האחרונות בקובץ
```console
$ head months.txt
January
February
March
April
May
June
July
August
September
October
$ tail months.txt
March
April
May
June
July
August
September
October
November
December
```
### דגלים ###
* `-n` \ `--lines`  
  אם נרצה לראות כמות אחרת של שורות נשתמש בדגל הזה
  לדוגמה:  
  ```
  $ head months.txt -n 3
  January
  February
  March
  $ tail months.txt -n 3
  October
  November
  December
  ```

## `ls` (list) ##
פקודה להצגה של הקבצים והתיקיות באותה תקייה לדוגמה
```console
$ ls
my_file.cc
my_README
other_file.mp4
my_sub_directory
```

### דגלים ###
* `-R` \ `--recurcive`  
  הצגת כל הקבצים ותתי התיקיות בצורה ריקורסיבית:  
  ```console
  $ ls -R
  .:
  a.cc  b.cc  dir1  dir2
  
  ./dir1:
  file1.txt

  ./dir2:
  subdir_1 dir_2_file1.mp4
  
  ./dir2/subdir_1:
  file1.txt  file2.txt
  ```
  לפי הדוגמה במיקום בו פתחנו את הטרמינל קיימים שני תיקיות ושני קבצים, בתקייה אחת יש רק קובץ אחד ובתתקייה השנייה יש תיקייה וקובץ, בתוך התקייה שבתקייה השנייה יש שני קבצים.   
  
* `-a` \ `--all`  
  פקודה עם הדגל "הכל" תציג את כל הקבצים והתקיות כולל קבצים מוסתרים והתיקיות 
  `..`
  (תקיית האם)
  והתקייה 
  `.`
  (התיקייה עצמה)  
  לדוגמה: 
  ```console
  $ ls -a
  .
  ..
  my_file.cc
  my_README
  other_file.mp4
  my_sub_directory
  hidden_dir
  ```
* `-l`  
  תציג את הקבצים והתיקיות עם מידע עליהם, מה ניתן לעשות איתם, מתי נוצרו, מה הגודל שלהם, מי יצר אותם, האם הם תיקיות  
  לדוגמה:  
  ```console
  $ ls -l
  -rw-rw-rw- 1 hodvak hodvak 2012 Jan 23 10:26 my_file.cc
  -rwxr--r-- 1 hodvak hodvak 1550 Jan 23 10:25 my_README
  -rwxrwxrwx 1 hodvak hodvak  100 Jan 23 10:26 other_file.mp4
  drwxrwxrwx 1 hodvak hodvak 4096 Jan 23 10:25 my_sub_directory
  ```
  העמודה הראשונה מעניינת אותנו
  העמודה מתחלקת ל10
  אותיות  
  הראשונה שבהם אומרת איזה סוג קובץ זה:
  - '-' עבור קובץ רגיל
  - 'd' עבור תיקייה
  - 'l' קיצור דרך

  9 האותיות האחרות מתחלקות ל3 קבוצות של 3 אותיות,  
  הראשונה:  
  איזה הראשאות יש למי שיצר את הקובץ  
  השנייה:  
  איזה הראשאות יש לקבוצה של מי שיצר את הקובץ  
  השלישית:  
  איזה הרשאות יש למשתמשים אחרים  
  
  כל קבוצה כוללת 3 אותיות
  rwx
  - 'r' ניתן לקרוא את הקובץ
  - 'w' ניתן לערוך את הקובץ
  - 'x' ניתן להריץ את הקובץ

## `touch` ##
הפקודה תיצור קובץ חדש אם הוא לא קיים ואם הוא כן קיים היא תשנה את הזמן האחרון שבו הוא נערך לזמן בו מפעילים את הפקודה.  
לדוגמה:  
```console
$ ls
$ touch a.cc
$ ls
a.cc
```
לפי הדוגמה בהתחלה לא היו קבצים בתקיייה ולאחר הפעולה נוצר קובץ בתקייה

## `file` ##
הפקודה תבדוק איזה סוג קובץ.  
לפקודה לא אכפת מסיומת הקובץ, היא בודקת מה הסוג לפי קריאה שלו
```console
$ ls
ex1a.cc
ex1a
README
dir
$ file ex1a.cc
ex1a.cc: C source, ASCII text
$ file ex1a
ex1a: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=1b0260a095902e1feec798a1204a4eda8dcd9bf6, not stripped
$ file README
README: ASCII text
$ file dir
dir: directory
```

## `chmod` (change mode) ##
שינוי מצב הרשאות של קובץ.  
את השינוי ניתן לעשות בצורה מספרית בצורה מילולית ובעזרת העתקה של הרשאות מקובץ אחר. נעבור על שלושת האופציות
### מילולית ###
כמו שכתבנו בהסבר על `ls` ההרשאות מתחלקות ל3
* \[u\]ser
* \[g\]roup
* \[o\]ther  

הדבר הרשאון שנכתוב זה את ההתחלה של השם של הקבוצות עליהם נרצה לעשות את השינוי,  
אם מדובר בכל הקבוצות ניתן לכתוב  
a (all)  
לאחר מכן נכתוב האם מוסיפים הרשאות, מורידים הרשאות או ששמים בדיוק את ההרשאות שנרצה בעזרת הסימנים +-=
לאחר מכן נכתוב את ההרשאות עצמם
לדוגמה:  
```console
$ ls -l
-rwxrwxrwx ... my_file.cc
$ chmod u=r my_file.cc
$ ls -l
-r--rwxrwx ... my_file.cc
$ chmod u=wr my_file.cc
$ ls -l
-rw-rwxrwx ... my_file.cc
$ chmod ug=wr my_file.cc
$ ls -l
-rw-rw-rwx ... my_file.cc
$ chmod a=x my_file.cc
$ ls -l
---x--x--x ... my_file.cc
$ chmod ug+w my_file.cc
$ ls -l
--wx-wx--x ... my_file.cc
$ chmod go-x my_file.cc
$ ls -l
--wx-w---- ... my_file.cc
```
### מספרית ###
כמו שכתבנו בהסבר על `ls` ההרשאות מתחלקות ל3
* \[u\]ser
* \[g\]roup
* \[o\]ther  

עבור כל אחד מהם נכתוב מספר בין 0 ל 7, 
כאשר כל ספרה בינארית מתכוונת להרשאה מסויימת
* 1 - x
* 2 - w
* 4 - r

אם נרצה לדוגמה הרשאות של קריאה והרצה של תוכנית, נשתמש ב  
1+4=5
```console
$ ls -l
-rwxrwxrwx ... my_file.cc
$ chmod 124 my_file.cc
$ ls -l
---x-w-r-- ... my_file.cc
$ chmod 657 my_file.cc
$ ls -l
-rw-r-xrwx ... my_file.cc
```

### העתקה מקובץ אחר ###
בשביל להעתיק מקובץ אחר נשתמש בדגל  
`--refrence`
אנחנו מאמינים שדוגמה תסביר בצורה הטובה ביותר
```console
$ ls -l
-r--rwxrwx ... my_file1.cc
-rwx---rwx ... my_file2.cc
$ chmod --refrence=my_file2.cc  my_file1.cc
$ ls -l
-rwx---rwx ... my_file1.cc
-rwx---rwx ... my_file2.cc
```

## `tree` ##
הצגת כל התיקיות והקבצים כל תתי התיקיות באותה תיקייה
מראה בצורה יותר יפה את מה שהיה ניתן לראות בעזרת הפקודה:  
`ls -R`
לדוגמה
```console
$ tree
.
├── a.cc
├── b.cc
├── dir
│   ├── file1.txt
│   └── file2.txt
└── dir2
    └── dir
        ├── file1.txt
        └── file2.txt
```
לפי מה שאנחנו רואים בדוגמה, בתיקייה שלנו יש 2 קבצים ושני תיקיות, באחת התקיות יש שני קבצים ובתקייה השניה יש תיקייה ובה 2 קבצים

## `sort` ##
סידור של כל השורות בקובץ שמקבלים.  
לדוגמה
```
$ cat months.txt
January
February
March
April
May
June
July
August
September
October
November
December
$ sort months.txt
April
August
December
February
January
July
June
March
May
November
October
September
```
בקובץ יש את שמות החודשים לפי הסדר שלהם בשנה.  
כאשר מפעילים את הפקודה על הקובץ מקבלים את שמות מסודרים בסדר סדר לקסיקוגרפי.  
### דגלים ###
* `-r` \ `--reverse`
  החזרת הערכים בסדר הפוך
  ```console
  $ sort months.txt
  September
  October
  November
  May
  March
  June
  July
  January
  February
  December
  August  
  April
  ```
* `-n` \ `--numeric-sort`
  מיון לפי מספר (כדי ש10 יגיע לפני 9 ואחרי 90)
  ```console
  $ sort grades.txt
  10
  100
  9
  90
  $ sort grades.txt -n
  9
  10
  90
  100
  ```
* `-k` \ `--key`
  :אם קימות עמודות ניתן לסדר לפי עמודה מסויימת לדוגמה
 
  ```console
  $ sort singers.txt
  Bob     Dylan
  Bob     Marley
  Elvis   Presley
  Freddie Mercury
  John    Lennon
  Michael Jackson
  $ sort -k2 singers.txt
  Bob     Dylan
  Michael Jackson
  John    Lennon
  Bob     Marley
  Freddie Mercury
  Elvis   Presley
  ```


## `grep` ##
על מנת לחפש ביטוי רגיל בקובץ נשתמש ב  
`grep`
```console
$ grep Ju months.txt
June
July
$ grep er months.txt
September
October
November
December
```
ניתן גם להכניס ביטוי רגולרי לחיפוש:
```console
$ grep '^[Mm]ay$' months.txt
May
```
### דגלים ### 
* `-n` \ `--line-number`
  הוספת מספר השורה ליד השורה אותה מציגים
  ```console
  $ grep er months.txt -n
  9:September
  10:October
  11:November
  12:December
  ```
* `-A` \ `--after-context`
  מדפיס מספר שורות אחרי השורה שנמצאה
  ```console
  $ grep may months.txt -A 2
  May
  June
  July
  ```
* `-B` \ `--before-context`
  מדפיס מספר שורות לפני השורה שנמצאה
  ```console
  $ grep may months.txt -B 2
  March
  April
  May
  ```
* `-C` \ `--context`
  מדפיס מספר שורות לפני ואחרי השורה שנמצאה
  ```console
  $ grep may months.txt -C 2
  March
  April
  May
  June
  July
  ```
## `mkdir` (make directory) ##
יצירת תקייה במקום בו הטרמינל פתוח  
אופן השימוש:  
```console
$ mkdir {new folder name}
```
דוגמה:
```console
$ ls
subdir1
subdir2
file1
$ mkdir newsubdir
$ ls
newsubdir
subdir1
subdir2
file1 
```
מה שקורה בעצם בדוגמה הוא שאנחנו בודקים איזה קבצים ותיקיות יש ומקבלים 3 קבצים ותיקיות.  
לאחר מכן אנחנו יוצרים תיקייה חדשה   
כאשר אנחנו בודקים שוב איזה קבצים ותיקיות קיימים אנחנו רואים שהתיקייה החדשה קיימת

## `less` ##
הצגת תוכן קובץ כלשהו  
נפעיל את הפעולה כך:  
```console
$ less {filename}
```
אם הקובץ ארוך, לחיצה על מקש האנטר יציג את השורה הבאה. לחיצה על רווח תציג את הדף הבא. כדי לצאת מהתצוגה של הקובץ יש ללחוץ על `q`
(quit)

## `cp` (copy) ##
העתקת קובץ אחד לקובץ חדש
```console
$ cp {old file} {new file}
```
הפעולה תעתיק את תוכן הקובץ הראשון ותיצור קובץ חדש בשם של הקובץ השני עם אותו תוכן שיש בקובץ הראשון.  
לדוגמה:  
```console
$ ls
my_file.txt
$ cp my_file.txt new_file.txt
$ ls
my_file.txt
new_file.txt
```
בדוגמה קיים רק קובץ אחד בתקייה ולאחר העתקה נוצר קובץ חדש.  
ניתן לבדוק ששני הקבצים באמת זהים באמצעות הפעולה `less`

### דגלים ###
* `-f` \ `--force`  
  אם הדגל "בכוח" לא קיים וקיים כבר הקובץ שאליו רוצים להעביר ואין הרשאות כתיבה לאותו קובץ הפעולה לא תפעל. ניתן דוגמה להסבר:  
  ```console
  $ ls -l
  -rwxrwxrwx 1 hodvak hodvak   12 Jan 23 10:26 a.cc
  -r-xr-xr-x 1 hodvak hodvak   12 Jan 23 13:12 b.cc
  $ cp a.cc b.cc
  cp: cannot create regular file 'b.cc': Permission denied
  $ cp -f a.cc b.cc
  $
  ```
  בדוגמה אין אפשרות לכתוב לקובץ
  b.cc  
  כאשר מנסים לעשות העתקה של הקובץ
  a.cc
  לקובץ
  b.cc
  הפעולה אומרת שיש בעיה והיא לא יכולה לעשות את זה  
  כאשר מנסים את אותה פעולה עם 
  -f
  הפעולה מצליחה
* `-r` \ `-R` \ `--recurcive`  
  העתקת תיקייה וכל הקבצים שבה בצורה ריקורסיבית  
  לדוגמה:  
  ```console
  $ tree
  .
  └── dir
      ├── file1.txt
      └── file2.txt
  $ cp -r dir dir2
  $ tree
  .
  └── dir
  │   ├── file1.txt
  │   └── file2.txt
  └── dir2
      ├── file1.txt
      └── file2.txt
  ```
  בדוגמה הייתה לנו תיקייה ובתוכה שני קבצים, העתקנו את התיקייה כולל כל הקבצים שבתוכה ותתי התיקיות שלה לתקייה חדשה
  



## `mv` (move) ##
שינוי שם של קובץ באופן דומה לפעולת ה`cp`  
```console
$ mv {old name} {new name}
```
פעולה זאת יכולה לשנות גם שמות של תיקיות וגם יכולה לשנות קבצים שאין הרשאה לערוך אותם 

## `rm` (remove) ##
מחיקת קובץ באופן דומה לפעולת ה`cp` 
```console
$ rm {file}
```
לפעולת הrm אותם דגלים כמו לפעולת ה cp

## `g++` ##
הפעולה אשר מקמפלת את הקבצים שלנו מקבצי קוד לקבצי הרצה  
אופן השימוש בפעולה:  
```console
$ g++ {code files}
```
אם נרצה גם להוציא קובץ הרצה נשתמש בדגל `-o`   
(output) ולאחריו שם קובץ ההרצה
לדוגמה:  
```console
$ g++ ex1a.cc -o ex1a
```
הפעולה תקמפל את הקובץ  `ex1a.cc` ותיצור קובץ הרצה בשם `ex1a`

כאשר אנחנו מקמפלים אנחנו משתמשים גם בדגל
`-Wall`  
(warning all)  
הדגל הזה אומר שהקומפיילר יקמפל כרגיל את התוכנית אבל אם משהו נראה לו חשוד, לדוגמה משתנים שלא משתמשים בהם, הוא יעיר לנו על כך  
כלומר קימפול תוכנית יראה כך:  
```console
$ g++ ex1a.cc -Wall -o ex1a
```
אם אנחנו משתמשים בספריות, בקבצי קוד נוספים שנרצה לקמפל לקובץ ההרצה שלנו, נוסיף אותם ליד קבצי הקוד (לדוגמה בתרגיל 5c)
```console
$ g++ ex5c.cc histogram.cc -Wall -o ex5c
```

לאחר יצירת קובץ ההרצה, ניתן להריץ את הקובץ על ידי פנייה למיקום בו נמצא הקובץ  
לדוגמה אם הוא נמצא באותה תיקייה
```console
$ ./ex1a
hello world
```
## `tar` (tape archive) ##
הפקודה יודעת לקבץ ולחלץ קבצים. 
### דגלים ###
* `-f` \ `--file`  
  שם הקובץ המקובץ

* `-c` \ `--create`  
  יצרת קובץ חדש מקבצים שמתקבלים
  ```console
  $ tar -c file1.cc file2.cc -f new_file.tar
  ```
  הפקודה תיצור קובץ חדש בשם  
  `new_file.tar`  
  שבו נמצאים הקבצים
  `file1.cc` ו `file2.cc`

* `-z` \ `--gzip`  
  שימוש בטכנולוגית קיבוץ אחרת שנקראת `gzip`

  יצרת קובץ חדש מקבצים שמתקבלים
  ```console
  $ tar -czf new_file.tar.gz file1.cc file2.cc
  ```
  הפקודה תיצור קובץ חדש בשם  
  `new_file.tar.gz`  
  שבו נמצאים הקבצים
  `file1.cc` ו `file2.cc`  
  הפעם בטכנולוגית  
  `gzip`

* `-x` \ `--extract` \ `--get`
  חילוץ הקבצים
  ```console
  $ tar -xf new_file.tar
  $ tar -xzf new_file.tar.gz
  ```
  שימו לב שאם היה  
  `z`  
  בפקודת הקיבוץ צריך שיהיה גם בפקודת החילוץ

* `-t` \ `--list`
  הצגת כל הקבצים בקובץ מקובץ
  ```console
  $ tar -t --file new_file.tar
  file1.cc
  file2.cc
  ```

### מאוחדים ומובצים ###
סיומות של קבצים מקובצים יהיו בדרך כלל  
`.tar.gz` או `.tgz`  
סיומות של קבצים מאוחדים יהיו בדרך כלל  
`.tar`  


## `diff` ##
הפקודה diff תראה את ההבדל בין שני קבצים.  
לדוגמה קיימים שני קבצים:  
a.txt:
```
hey,
my
name
is
Hod
```
b.txt:
```
hey,
your
name
is
Ron
```
```console
$ diff a.txt b.txt
2c2
< my
---
> your
5c5
< Hod
---
> Ron
```
השורה הראשונה אומרת בין איזה שורות יש הבדל.  
השורה השנייה מראה את השורה בקובץ הראשון.  
השורה השלישית היא שורת הפרדה.  
השורה הרביעית מראה את השורה בקובץ השני. 


## `yppasswd` ##
פקודה לשינוי סיסמת הלינוקס.  
פועלת באופן הבא:  
```console
$ yppasswd
> {current password}
> {new password}
> {new password autorize}
```
לאחר כתיבת הפקודה `yppasswd` המערכת תבקש את הסיסמה הישנה ופעמיים את הסיסמה החדשה.  
אם הכל הוקש כראוי, הסיסמה תשתנה
את הסיסמאות לא תראו על המסך אך הם נכתבות

## `wc` (word counter) ##
הפעולה תחזיר את כמות המילים, כמות השורות וכמות האותיות בקובץ  
```console
$ cat text.txt
Hello my name
is
Hod
$ wc text.txt
 3  5 21 text.txt
```

## `spell` ##
מחזירה את כל המילים שיש בהם שגיאות
```console
$ spell
> this comand is so efective
comand
efective
```
ניתן גם לבדוק קובץ
```console
$ spell months.txt
Mey
Ogust
```

## `du` (disk usage) ##
יחזיר עבור כל תקייה את הגודל שלה
```console
$ du
100      ./dir
200      ./my_dir2/subdir
250      ./my_dir2
400      .
```
ניתן גם להכניס שם של תקייה
```console
$ du my_dir2
200      my_dir2/subdir
250      my_dir2
```

### דגלים ###
* `-s` \ `--summarize`
  הצגת הגודל של התקייה לבד בלי תתי התיקיות
  ```console
  $ du -s
  400      .
  ```

* `-h` \ `--human-readable`
  מדפיס את הגודל של תיקיות כך שיהיה קל לקרוא אותם
  ```console
  $ du -h
  100K      ./dir
  200K      ./my_dir2/subdir
  250K      ./my_dir2
  400K      .
  ```
  * K - Killobyte
  * M - Megabyte

## `ps` ##
מראה את התהליכים שרצים
לכל תוכנית יש מספר יחודי שנקרא  
PID (Process Identifier)  
שעליו נדבר בפקודה הבאה
```console
$ ps
  PID TTY          TIME CMD
  934 tty2     00:00:00 bash
  983 tty2     00:00:00 ps
```
### דגלים ###
* `-aux`  
  מראה את כל התהליכים שרצים
  מראה אותם גם עם שמות משתמשים ויותר מידע

## `kill` ##
מקבלת PID ועוצרת את אותו תהליך 
```console
$ ps -u
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
hodva      68091  0.0  0.0  54408  6852 pts/5    Ss   11:07   0:00 -csh
hodva      77865  0.1  0.0  54416  6892 pts/7    Ss   11:33   0:00 -csh
hodva      78824  0.0  0.0   7312   824 pts/5    S+   11:35   0:00 sleep 100
hodva      78844  0.0  0.0  77316  6068 pts/7    R+   11:35   0:00 ps -u
$ kill 78824
$ ps -u
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
hodva      68091  0.0  0.0  54408  6852 pts/5    Ss   11:07   0:00 -csh
hodva      77865  0.1  0.0  54416  6892 pts/7    Ss   11:33   0:00 -csh
hodva      78850  0.0  0.0  77316  6068 pts/7    R+   11:35   0:00 ps -u
```

## `top` ##
הפקודה תציג את התהלכים שצורכים הכי הרבה משאבים:
```
$ top
top - 15:53:57 up 1 day,  4:56,  0 users,  load average: 0.52, 0.58, 0.59
Tasks:   7 total,   1 running,   6 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.7 us,  3.0 sy,  0.0 ni, 94.7 id,  0.0 wa,  1.5 hi,  0.0 si,  0.0 st
MiB Mem :   8062.8 total,   1136.1 free,   6702.7 used,    224.0 buff/cache
MiB Swap:  24576.0 total,  19798.5 free,   4777.5 used.   1229.5 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 root      20   0    8940    144    100 S   0.0   0.0   0:00.09 init
  165 root      20   0    8948     76     36 S   0.0   0.0   0:00.00 init
  166 hodva     20   0   18232   1464   1408 S   0.0   0.0   0:00.27 bash
  179 root      20   0    8948     76     36 S   0.0   0.0   0:00.00 init
  180 hodva     20   0   18364     60     56 S   0.0   0.0   0:00.67 bash
  270 hodva     20   0   18920   2144   1528 R   0.0   0.0   0:00.01 top
  271 hodva     20   0   15420    936    664 S   0.0   0.0   0:00.01 cat
```
## `date` ##
פקודה שמראה את זמן המחשב  
דוגמה:  
```console
$ date
Wed Jan 12 14:36:50 IST 2022
```

## `cal` (calendar) ##
מראה את התאריך בלוח שנה:  
```console
$ cal
    February 2022   
Su Mo Tu We Th Fr Sa
       1  2  3  4  5
 6  7  8  9 10 11 12
13 14 15 16 17 18 19
20 21 22 23 24 25 26
27 28    
$ cal 5 june 2000
   June 2000     
Su Mo Tu We Th Fr Sa
             1  2  3
 4  5  6  7  8  9 10
11 12 13 14 15 16 17
18 19 20 21 22 23 24
25 26 27 28 29 30 
```

### `find` ###
מוצא את המיקום של קובץ שמחפשים
```console
$ tree
.
├── a.cc
├── b.cc
├── dir
│   ├── file1.txt
│   └── file2.txt
└── dir2
    └── dir
        └── this_one.txt
$ find . -name this_one.txt
./dir2/dir/this_one.txt
$ find . -name 'file?.txt'
./dir/file1.txt
./dir/file2.txt
```

## `history` ##
מחזיר את כל הפקודות שאי פעם הרצנו עם מספר סידורי לכל אחת לדוגמה
```console
$ ls
text.txt
$ touch text2.txt
$ ls
text.txt
text2.txt
$ history
   1  12:11	touch a.txt
   2  12:11	ls
   3  12:11	history
```
ניתן להריץ את אחת הפקודות בערת סימן קריאה והמספר שלה, לדוגמה
```console
$ history
   1  12:11	touch a.txt
   2  12:11	ls
   3  12:11	history
$ !2
text.txt
text2.txt
```
אם נכתוב סימן קריאה וטקסט כלשהו נקבל את הפקודה האחרונה שהתחילה באותה צורה
```console
$ history
   1  12:11	rm text.txt
   2  12:11	rm a.txt
   3  12:11	touch text.txt
   4  12:11	touch a.txt
   5  12:11	history
$ ls
a.txt
text.txt
$ !rm
$ ls
text.txt
```

# Pipe Line #
כלי חזק שמאפשר הכנסת פלט של פקודה אחת כקלט לפקודה אחרת.  
רוב הפקודות מאפשרות את זה  
לדוגמה
```console
$ ls
dir
dir2.txt
file.txt
file2.txt
file_in_dir.txt
$ ls | grep dir
dir
dir2
$ man sort | grep '\-\-reverse' -A 2
       -r, --reverse
              reverse the result of comparisons
```
בפקודה הראשונה בדקנו מה הם כל הקבצים
בפקודה השנייה בדקנו מה הם כל הקבצים וסיננו רק את אלו שכוללים את המילה  
'dir'
בפקודה השלישית בדקנו מה הדגל   
`--reverse`  
עושה בפקודה sort  
