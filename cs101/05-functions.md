# פונקציות #
פונקציה היא קטע קוד שנרצה להריץ ונוכל לקרוא לפונקציה מכל מקום בקוד. 


## פונקציה בסיסית ##
פונקציה היא קטע קוד שנרצה להריץ ונוכל לקרוא לפונקציה מכל מקום בקוד.  
לפונקצויות קיימים שלושה חלקים.  

### הגדרת הפונקציה ###
את הפונקציה הבסיסית נגדיר כך:
```cpp
void func_name()
{
    {code...}
}
```
לדוגמה:
```cpp
void say_hello_5_times()
{
    for(int i = 0; i < 5; i ++)
    {
        cout << "hello" << endl;
    } 
}
```
בדוגמה יצרנו פונקציה שמדפיסה hello 5 פעמים

### חתימה ###
בתחילת הקוד. נוסיף סקשיון שבו יהיו חתימות כל הפונקציות על מנת שנוכל להשתמש בהם מכל מקום בקוד.
החתימה תיראה כך:
```cpp
\\----prototypes section-----
void func_name();
```
דוגמה:  
```cpp
//----prototypes section----
void say_hello_5_times();

//-----------main-----------
int main()
{
    return EXIT_SUCCESS;
}
//--------------------------
void say_hello_5_times()
{
    for(int i = 0; i < 5; i ++)
    {
        cout << "hello" << endl;
    } 
}
```

### קריאה לפונקציה ###
על מנת להשתמש בפונקציה, אנחנו צריכים לקרוא לה.
קריאה לפונקציה מתבצעת כך:
```cpp
func_name();
```
לדוגמה:  
```cpp
//----prototypes section----
void say_hello_5_times();

//-----------main-----------
int main()
{
    say_hello_5_times();
    cout << "hey" << endl;
    say_hello_5_times();

    return EXIT_SUCCESS;
}
//--------------------------
void say_hello_5_times()
{
    for(int i = 0; i < 5; i ++)
    {
        cout << "hello" << endl;
    } 
}
```
בדוגמה קראנו לפונקציה פעמיים ובין שניהם הדפסנו hey והפלט יראה כך:

```
hello
hello
hello
hello
hello
hey
hello
hello
hello
hello
hello
```

## פונקציה עם פרמטרי ערך ##
לפעמים נרצה לקבל מידע ולפיו לעשות קטע קוד מסויים, למידע שנקבל ולפיו נעשה את הפעולה קוראים פרמטרים. הגדרת פעולה עם פרמטרים נראית כך:  
```cpp
void func_name(type1 name1, type2 name2...)
{
    {code...}
}
```
דוגמה:
עכשיו נרצה פעולה גם שמדפיסה כמות מסויימת של פעמים
hello  
איך נכתוב את זה?   
```cpp
void say_hello_x_times(int num_of_times)
{
    for(int i = 0; i < num_of_times; i ++)
    {
        cout << "hello" << endl;
    } 
}
```
החתימה של הפונקציה משתנה בהתאמה:  
```cpp
void say_hello_x_times(int num_of_times);
```
וגם הקריאה לפונקציה משתנה.  
חייבים לקרוא לפונקציה עם ערך כלשהו.   
לדוגמה:  
```cpp
say_hello_x_times(10);
```
ניתן גם להשתמש בביטוי:  
```cpp
say_hello_x_times((10-5)*2);
```
אפשר אפילו להשתמש במשתנים בביטוי:  
```cpp
int a = 4;
say_hello_x_times(a*3);
```
דוגמה מלאה לשימוש:
```cpp
//----prototypes section----
void say_hello_x_times(int num_of_times);

//-----------main-----------
int main()
{
    int x=4;
    say_hello_x_times(x);
    cout << "hey";
    say_hello_x_times(2); 
    return EXIT_SUCCESS;
}

void say_hello_x_times(int num_of_times)
{
    for(int i = 0; i < num_of_times; i ++)
    {
        cout << "hello" << endl;
    } 
}
```
הפלט יהיה:  
```
hello
hello
hello
hello
hey
hello
hello
```

נמציא את הפונקציה וגאס ונראה מה קורה איתה.
```cpp
//----prototypes section----
void vegas(int a, int b);

//-----------main-----------
int main()
{
    int a = 3;
    int b = 4;
    vegas(a,b);
    cout << a << " " << b << endl;
}

void vegas(int a, int b)
{
    int old_a = a;
    a = b;
    b = old_a;
}
```
מה שקורה בפונקציה וגאס זה שa וb מתחלפים.  
ולמרות הפלט יהיה:  
```
3 4
```
למה זה קורה? כי מה שקורה בפונקציה נשאר בפונקציה. 
(ואצלינו בתוכנית מה שקורה בוגאס נשאר בוגאס)  
כל שינוי של משתנים בפונקציה לא ישנה אותם אצל מי שקרא להם.

## פונקציה עם פרמטרי הפניה ##
אם בכל זאת נרצה לשנות ערכים של משתנים שקיבלנו בפונקציה, נשתמש בפרמטרי הפניה, כלומר נשים & לפני שם המשתנה:  
עכשיו אם נשנה את הערך של הפרמטר הערך ישתנה גם במשתנה שנקרא עם הפונקציה.  
```cpp

//----prototypes section----
void swap(int &a, int &b);

//-----------main-----------
int main()
{
    int a = 3;
    int b = 4;
    swap(a,b);
    cout << a << " " << b << endl;
}

void swap(int &a, int &b)
{
    int old_a = a;
    a = b;
    b = old_a;
}
```
עכשיו הפלט באמת יהיה
```
4 3
```

**חשוב להדגיש**
אם משתמשים בפרמטרי הפנייה, אי אפשר להכניס ביטיים שמחזירים ערך, רק משתנים. לדוגמה עבור:  
```cpp
void zero_it(int &a)
{
    a = 0;
}
```

```cpp
int b = 1;
zero_it(10); //wrong
zero_it(b+1); //wrong
zero_it(b); //right
```

ניתן להשתמש בפרמטרי הפנייה כדי להחזיר מידע, לדוגמה נבנה פעולה שמחזירה ערך של כפל:  
```cpp
void mul(int num1,int num2, int &res)
{
    res = num1*num2;
}
```
שימוש בפונקציה:
```cpp
int a = 5;
int b = 6;
int c;

mul(a,b,c);
cout << c;
```

מה שיקרה זה שבגלל שניתן לשנות ערך משנים את הערך של res להיות הערך של כפל של המספר הראשון במספר השני.   
הפלט יהיה:  
```
30
```


## פונקציה שמחזירה מידע ##
ניתן גם ממש "להחזיר" מידע בעזרת פונקציה. נרצה לקרוא לפונקציה mul לדוגמה, בצורה הבאה:
```cpp
int a = 5;
int b = 6;
int c;

c = mul(a,b);
cout << c;
```
איך נעשה את זה, דבר ראשון נגיד לפונקציה שהיא מחזירה משהו, נחליף את הvoid בסוג של מה שאנחנו נרצה להחזיר (במקרה שלנו אנחנו מחזירים מספר שלם ולכן int)  
```cpp
int mul(int num1, int num2)
{
    {code...}
}
```
כדי להחזיר ערך, נשתמש בreturn:

```cpp
int mul(int num1, int num2)
{
    return num1*num2;
}
```
קוד מלא לדוגמה:

```cpp
//----prototypes section----
int mul(int num1, int num2);

//-----------main-----------
int main()
{
    int a = 3;
    int b = 4;
    cout << mul(1,4) << " " << mul(3,b)<< " " << mul(a,a) << " "<< mul(a+2,1) << endl;
}

int mul(int num1, int num2)
{
    return num1*num2;
}
```
הפלט של הקוד יהיה:  
```
4 12 9 5
```

